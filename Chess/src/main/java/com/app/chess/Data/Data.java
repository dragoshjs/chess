package com.app.chess.Data;

import com.app.chess.Figures.Figure;

import java.util.LinkedList;

public class Data {
    public static final Figure[][] figuresArray = new Figure[8][8];
    public static final LinkedList<Figure> figuresList = new LinkedList<>();
}
